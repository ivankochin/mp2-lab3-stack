#ifndef __TSIMPLESTACK_H__
#define __TSIMPLESTACK_H__

#define StackSize 30
#include <iostream>
using namespace std;

template <class StackClass>
class TSimpleStack{
	private:
		StackClass Mem[StackSize];
		int end;

	public:
		
		//����������� �� ���������
		TSimpleStack(): end(-1){}
			//�������� � ���� ��������
		void Push(StackClass s)
		{
			try
			{
		
				if (IsFull()) throw 2;
					else  Mem[++end ]=s;
			}
			//����� ����������
			catch(int error)
			{
				if(error ==2) cout<<"Stack is Full"<<endl;
			}
			
		}
			//������� �� ����� ��������
		StackClass Pop()
		{
			try
			{
				if (IsEmpty()) throw 1;
					else  return(Mem[end--]);
			}
			//����� ����������
			catch(int error)
			{
				if(error ==1) cout<<"Stack is Empty"<<endl;
			}
		}
			//�������� �� �������������
		bool IsFull()
		{
			return(end==StackSize-1);
		}
		//�������� �� �������
		bool IsEmpty()
		{
			return(end==-1);
		}
		//������ ��� �������
		int GetEnd()
		{
		return end;
		}

};

#endif