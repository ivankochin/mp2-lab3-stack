#include <gtest/gtest.h>
#include "expressions.h"

TEST(BrControl, can_check_parentheses_placement)
{
    EXPECT_EQ(true, ErrorCheck("(1+2)/(3+4*6.7)-5.3*4.4", false));
}

TEST(BrControl, can_detect_incorrect_placement_of_parentheses)
{
    EXPECT_EQ(false, ErrorCheck("(a+b1)/2+6.5)*(4.8+(", false));
}

TEST(ConvertToPostfix, can_convert_an_expression_with_int_nums)
{
    EXPECT_EQ(Transform("3*(1-5)+7"), "315-*7+");
}

TEST(ConvertToPostfix, not_covnert_an_exp_with_incrrt_placement_of_pars)
{
    ASSERT_ANY_THROW(Transform("2*(8-5)+4)"));
}

TEST(Calculation, can_calculate_a_expression)
{
    int res = (1+2)/(3+4*6)-5*4;
    EXPECT_DOUBLE_EQ(Calculation("(1+2)/(3+4*6)-5*4"), res);
}

TEST(Calculation, not_calculate_an_exp_with_incorrect_placement_of_pars)
{
    ASSERT_ANY_THROW(Calculation("(1+2)(/())3+4*6.7)-5.3*4.4)("));
}