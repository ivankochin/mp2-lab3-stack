#pragma once
#include "TStack.h"
#include <iostream>
#include <string>

#define MAXSTRSIZE 256

using namespace std;

bool IsOperation(const char& ch){
	if ((ch=='(')||(ch==')')||(ch=='+')||(ch=='-')||(ch=='*')||(ch=='/'))
		return true;
	return false;

}
int Priority(const char& ch){
	switch (ch)
	{
	case '(':return 0;
	case ')':return 1;
	case '+':return 2;
	case '-':return 2;
	case '*':return 3;
	case '/':return 3;
	
	default: return -1;
	}


}
//((1+31)/2+6.5)*(4.8+5)
int ErrorCheck (const string &str, bool print){
	TStack BracStack(MAXSTRSIZE);
	int ArrBrac [MAXSTRSIZE][2]={0};
	int ArrCoun =0; //���� ����� �������, ������� �� ������ ���������
	int BracCoun=1;//������ ������ ������������� �����
	int i=0;
	int ErrCoun=0;//������� ������
	while (str[i]){

		
		if(str[i]=='('){
			BracStack.Put(BracCoun);
			BracCoun++;
			
			
		}
		else if(str[i]==')'){
			
			if (!BracStack.IsEmpty()){
				ArrBrac[ArrCoun][0]=BracStack.Get();
				ArrBrac[ArrCoun][1]=BracCoun;

				//cout<<ArrBrac[ArrCoun][0]<<" "<<ArrBrac[ArrCoun][0]<<endl;
				BracCoun++;
				ArrCoun++;
			}else{
			ArrBrac[ArrCoun][0]=0;
			ArrBrac[ArrCoun][1]=BracCoun;
			BracCoun++;
			ArrCoun++;
			ErrCoun++;
		}}
		
		
		

		
		
		i++;
	}
	
	while(!BracStack.IsEmpty()){
			ArrBrac[ArrCoun][0]=BracStack.Get();
			ArrBrac[ArrCoun][1]=0;
			ArrCoun++;
			ErrCoun++;
		}
		
		if(print){
			if(ErrCoun!=0)
				cout<<ErrCoun<<" Bracket's errors"<<endl;
			else 
				cout << "Bracket errors not found" << endl;


			i=0;
			cout <<"|Open Brackets|\t|Close Brackets|"<<endl;
			while (i<ArrCoun){
				cout <<"\t"<<ArrBrac[i][0]<<"\t\t"<<ArrBrac[i][1]<<endl;
				i++;
			}


		}
		
		return ErrCoun;
}


string Transform(const string &str){
	if(ErrorCheck(str,false)==0){
	
	int i=0;
	TStack OpStack(MAXSTRSIZE);
	string result;
	while (str[i]){
		//���� ��������
		if(IsOperation(str[i])){
			if (str[i]=='('){//���� ������������� ������, �� ������ � ����
				OpStack.Put(str[i]);
			}else if (str[i]==')'){//���� �������������, �������� ��� �� ���� ������
				while(true){
					if (OpStack.ShowLast()=='('){
						OpStack.Get();
						break;
					}
					result+=(char)OpStack.Get();
				}
			
			}else if(OpStack.IsEmpty()){//���� ���� ����
				OpStack.Put(str[i]);
				
			}else if(Priority(str[i])>Priority((char)OpStack.ShowLast())){//���� ��������� �������� � ������ ���� ��� ����� ���������� ��������� �������� � �����
				OpStack.Put(str[i]);
				
			}else{//���� ��������� �������� � ����� ������, ��� � ������
				while(true){
				
					result+=(char)OpStack.Get();
					if(Priority((char)OpStack.ShowLast())<Priority(str[i])||OpStack.IsEmpty())
						break;
					}
				OpStack.Put(str[i]);
				}
			
			}
		else if(isdigit(str[i])){//���� �����, �� ������ ������ � ������
		result+=str[i];
		}
			i++;
	}
			
	//���� � ����� �������� ��������, �� ���� ������
	while (!OpStack.IsEmpty())
		result+=(char)OpStack.Get();
			
			
	return result;
	}else{
	throw 1;
	return "Error";
	}
}


//5 + 4 * (7 - 2)
int Calculation(const string & str){
	string post= Transform(str);
	TStack CalcStack(MAXSTRSIZE);
	int i=0;
	int FirstNum, SecNum;

	if(post!="Error"){
	while (post[i]){
		if(isdigit(post[i])){
			CalcStack.Put(post[i]-'0');
		}
		if(IsOperation(post[i])){
			SecNum = CalcStack.Get();
			FirstNum =CalcStack.Get();
			switch(post[i]){
			case '+':CalcStack.Put(FirstNum+SecNum);break;
			case '-':CalcStack.Put(FirstNum-SecNum);break;
			case '*':CalcStack.Put(FirstNum*SecNum);break;
			case '/':CalcStack.Put(FirstNum/SecNum);break;
				}
			}
		i++;
		}
	
	return CalcStack.Get();
	
	
	
	}else{
		throw 1;
	}
}