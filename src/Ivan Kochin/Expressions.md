# ������ ���������������� 2: ����


## ���� � ������

� ������ ������������ ������ ���� ���������� ������ ���������� ���� ����� ������:  

- ����������, ����������� �� ����������� ������� (����� `TSimpleStack`);
- ����� ��������, ����������� �� ������������� ������������ ��������� (����� `TStack`).

� ������� ������������� ������ ����� ���� �������� ����������, ������� ��������� �������������� ���������, �������� � ���� ������ � �������� �������������. ��������� ��������� ���������� ������ ������, �������������� ������ ����������� ������������� �����.

������ ����������� �� ������ �������-�������, ����������� ���������:

 - ���������� ������� `TDataCom` � `TDataRoot` (h-�����)
 - �������� ������ ������������� ������ `TStack`

����������� ���������� ������ �������� ������� ��������� �����:

  1. ���������� ������ `TSimpleStack` �� ������ ������� ������������� �����.
  1. ���������� ������� ������ `TDataRoot` �������� ��������� ����������.
  1. ���������� ������ `TStack`, ����������� ����������� ������� �� `TDataRoot`.
  1. ���������� ������ ��� �������� ����������������� ������.
  2. ���������� ��������� �������� ������������ ���������� ��������������� ���������.
  2. ���������� ���������� ������� � ���������� ��������������� ���������.
  1. ����������� ����������������� ������ � ������� �������������.

 __���������� ������ `TSimpleStack`:__

```c++
#ifndef __TSIMPLESTACK_H__
#define __TSIMPLESTACK_H__

#define StackSize 30
#include <iostream>
using namespace std;

template <class StackClass>
class TSimpleStack{
	private:
		StackClass Mem[StackSize];
		int end;

	public:
		
		//����������� �� ���������
		TSimpleStack(): end(-1){}
			//�������� � ���� ��������
		void Push(StackClass s)
		{
			try
			{
		
				if (IsFull()) throw 2;
					else  Mem[++end ]=s;
			}
			//����� ����������
			catch(int error)
			{
				if(error ==2) cout<<"Stack is Full"<<endl;
			}
			
		}
			//������� �� ����� ��������
		StackClass Pop()
		{
			try
			{
				if (IsEmpty()) throw 1;
					else  return(Mem[end--]);
			}
			//����� ����������
			catch(int error)
			{
				if(error ==1) cout<<"Stack is Empty"<<endl;
			}
		}
			//�������� �� �������������
		bool IsFull()
		{
			return(end==StackSize-1);
		}
		//�������� �� �������
		bool IsEmpty()
		{
			return(end==-1);
		}
		//������ ��� �������
		int GetEnd()
		{
		return end;
		}

};

#endif
```
__���������� ������� tdataroot__

```c++
// ����, ���, ���� "������ ����������������-2", �++, ���
//
// tdataroot.h - Copyright (c) ������� �.�. 28.07.2000 (06.08)
//   ������������ ��� Microsoft Visual Studio 2008 �������� �.�. (21.04.2015)
//
// ������������ ��������� ������ - ������� (�����������) ����� - ������ 3.2
//   ������ ���������� ����������� ��� �������� ������� SetMem

#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"

#define DefMemSize   25  // ������ ������ �� ���������

#define DataEmpty  -101  // �� �����
#define DataFull   -102  // �� �����������
#define DataNoMem  -103  // ��� ������

typedef int    TElem;    // ��� �������� ��
typedef TElem* PTElem;
typedef int    TData;    // ��� �������� � ��

enum TMemType { MEM_HOLDER, MEM_RENTER };//������������ MEM_HOLDER == 0, MEM_RENTER == 1

class TDataRoot: public TDataCom
{
protected:
  PTElem pMem;      // ������ ��� ��
  int MemSize;      // ������ ������ ��� ��
  int DataCount;    // ���������� ��������� � ��
  TMemType MemType; // ����� ���������� �������

  void SetMem(void *p, int Size);             // ������� ������
public:
  virtual ~TDataRoot();
  TDataRoot(int Size = DefMemSize);
  virtual bool IsEmpty(void) const;           // �������� ������� ��
  virtual bool IsFull (void) const;           // �������� ������������ ��
  virtual void  Put   (const TData &Val) = 0; // �������� ��������
  virtual TData Get   (void)             = 0; // ������� ��������

  // ��������� ������
  virtual int  IsValid() = 0;                 // ������������ ���������
  virtual void Print()   = 0;                 // ������ ��������

  // ������������� ������
  friend class TMultiStack;
  friend class TSuperMultiStack;
  friend class TComplexMultiStack;
};

#endif

```
__���������� ���������� tdataroot__
```c++
#include <stdio.h>
#include "tdataroot.h"

/*
PTelem - ��������� �� int ��� �������� �������
TData - int

PTelem PMem - ������ ��� �����
MemSize - ���� ���-�� ��������� (���-�� ���������� ������)
DataCount - ������� �����
MemType - MEM_HOLDER ��� MEM_RENTER ������ �� ����� ��������� ������
RetCode - ��� ����������
*/


/*��������������� �����������
RetCode == 0 �� ���������*/
TDataRoot::TDataRoot(int Size) :TDataCom(){
	if(Size<0)
		throw SetRetCode(DataNoMem);
	else{
	
		DataCount=0;
		MemSize = Size;
	if (Size==0){
		pMem=nullptr;
		MemType = MEM_RENTER;
	}
	else
	{
		pMem = new TElem [Size];
		MemType = MEM_HOLDER;
	}
		
	}
}

/*����������� �����������*/
TDataRoot::~TDataRoot(){
	delete [] pMem;
}

/*������� ������ ��� MEM_RENTER */
void TDataRoot :: SetMem(void *p, int Size) {
		if (Size < 0) 
			throw SetRetCode(DataNoMem);
		else if(MemType == MEM_HOLDER)
		{
			MemSize = Size;
			PTElem tmp = new TElem[MemSize];//������� ����� ������ ������� �������
			p = pMem;//��������� ������ ������ � void
			pMem = tmp;//����������� pMem ������ ������
			tmp = (PTElem)p; //�������� void � ������� ����
			for (int i = 0; i < DataCount; ++i)
			{
				pMem[i] = tmp[i];//����������� �����������
			}
			delete[] tmp;//������� ��������
		}else{
		MemSize = Size;
		for(int i =0;i<DataCount;++i){
			((PTElem)p)[i] = pMem[i];
		}
		pMem = (PTElem)p;
		
		}
}
/*������� �������� ������� � �������*/
bool TDataRoot::IsEmpty() const
{
	return DataCount==0;
}

bool TDataRoot::IsFull() const
{
	return DataCount==MemSize;
}

```

__���������� ������ `TStack`:__

���������:

```c++
#ifndef __TSTACK_H
#define __TSTACK_H

#include "tdataroot.h"
#include <stdio.h>
#include <iostream>


using namespace std;

class TStack: public TDataRoot{
	protected:
		int Top;
	public:
		TStack(int Size = DefMemSize): TDataRoot(Size), Top(-1){}
		virtual void Put(const TData &Val);
		virtual TData Get(void);
		virtual void Print();
		virtual int IsValid();
		int GetDataCount(){return DataCount;};
		TData ShowLast(void);
		};
```

������ ������:

```c++
TData TStack::Get(void)
{
	TData result = -1;
	if(pMem == NULL)
		SetRetCode(DataNoMem);
	else if (IsEmpty())
		SetRetCode(DataEmpty);
	else
	{
		result = pMem[Top--];
		DataCount--;
	}
	return result;
}
//���������� ����
void TStack:: Print ()
{
	if(IsEmpty())
	cout<<"Stack is Empty";
	for (int i=0;i<DataCount;i++)
		cout<<pMem[i]<<' ';

	cout<<endl;


}
int TStack:: IsValid()
{
	return 1;
}

TData TStack:: ShowLast(void){
	TData result = -1;
	if(pMem == NULL)
		SetRetCode(DataNoMem);
	else if (IsEmpty())
		SetRetCode(DataEmpty);
	else
	{
		result = pMem[Top];
	}
	return result;
}

#endif
```

__�����, ������������� ��� �������� ����������������� ������:__

��� 'TSimpleStack':

```c++
#include <gtest/gtest.h>

#include <TSimpleStack>

TEST(TSimpleStack, can_create_stack)
{
    ASSERT_NO_THROW(TSimpleStack s1());
}

TEST(TSimpleStack, new_stack_is_empty)
{
    TSimpleStack s1;
    EXPECT_EQ(true, s1.IsEmpty());
}

TEST(TSimpleStack, can_push_elem_in_stack)
{
    TSimpleStack s1;
    int elem = 1;
    ASSERT_NO_THROW(s1.Push(elem));
}

TEST(TSimpleStack, stack_with_elem_isnt_empty)
{
    TSimpleStack s1;
    int elem = 1;
    s1.Push(elem);
    EXPECT_EQ(false, s1.IsEmpty());
}

TEST(TSimpleStack, cant_push_in_full_stack)
{
    TSimpleStack s1;
    int i;
    for (i = 0; i < MemSize; i++)
        s1.Push(i);
    ASSERT_ANY_THROW(s1.Push(i + 1));
}


TEST(TSimpleStack, can_pop_elem_from_stack)
{
    TSimpleStack s1;
    int elem1 = 1, elem2;
    s1.Push(elem1);
    elem2 = s1.Pop();
    EXPECT_EQ(elem1, elem2);
}

TEST(TSimpleStack, cant_pop_from_empty_stack)
{
    TSimpleStack s1;
    ASSERT_ANY_THROW(s1.Pop());
}

TEST(TSimpleStack, pop_returns_last_pushed_elem)
{
    TSimpleStack s1;
    int elem1 = 1, elem2 = 2, res;
    s1.Push(elem1); s1.Push(elem2);
    res = s1.Pop();
    EXPECT_TRUE((res = elem2) && (res != elem1));
}

TEST(TSimpleStack, full_stack_after_pop_isnt_full)
{
    TSimpleStack s1;
    int i;
    for (i = 0; i < MemSize; i++)
        s1.Push(i);
    bool isf = s1.IsFull();
    int val = s1.Pop();
    EXPECT_TRUE(isf != s1.IsFull());
}
```

��� 'TStack':

```c++
#pragma once

#include <gtest/gtest.h>

#include <tdatstack.h>

TEST(TStack, can_create_stack)
{
    ASSERT_NO_THROW(TStack s1);
}

TEST(TStack, can_create_stack_with_set_size)
{
    ASSERT_NO_THROW(TStack s1(5));
}

TEST(TStack, new_stack_is_empty)
{
    TStack s1;
    EXPECT_EQ(true, s1.IsEmpty());
}

TEST(TStack, can_push_elem_in_stack)
{
    TStack s1;
    int elem = 1;
    ASSERT_NO_THROW(s1.Push(elem));
}

TEST(TStack, stack_with_elem_isnt_empty)
{
    TStack s1;
    int elem = 1;
    s1.Push(elem);
    EXPECT_EQ(false, s1.IsEmpty());
}

TEST(TStack, cant_push_in_full_stack)
{
    TStack s1;
    int i;
    for (i = 0; i < DefMemSize; i++)
        s1.Push(i);
    s1.Push(i + 1);
    EXPECT_EQ(DataFull, s1.GetRetCode());
}


TEST(TStack, can_pop_elem_from_stack)
{
    TStack s1;
    int elem1 = 1, elem2;
    s1.Push(elem1);
    elem2 = s1.Get();
    EXPECT_EQ(elem1, elem2);
}

TEST(TStack, cant_pop_from_empty_stack)
{
    TStack s1;
    s1.Get();
    EXPECT_EQ(DataEmpty, s1.GetRetCode());
}

TEST(TStack, pop_returns_last_pushed_elem)
{
    TStack s1;
    int elem1 = 1, elem2 = 2, res;
    s1.Push(elem1); s1.Push(elem2);
    res = s1.Get();
    EXPECT_TRUE((res = elem2) && (res != elem1));
}

TEST(TStack, full_stack_after_pop_isnt_full)
{
    TStack s1;
    int i;
    for (i = 0; i < DefMemSize; i++)
        s1.Push(i);
    bool isf = s1.IsFull();
    int val = s1.Get();
    EXPECT_TRUE(isf != s1.IsFull());
}
```




##���������� �������������� ���������

��� ������� ������ ������ ���� ������������� ���������� 'expressions', ���������� ��������� ��� ���������� �������������� ���������, ���������� �������.

 __����������:__  


```c++
#pragma once
#include "TStack.h"
#include <iostream>
#include <string>

#define MAXSTRSIZE 256

using namespace std;

bool IsOperation(const char& ch){
	if ((ch=='(')||(ch==')')||(ch=='+')||(ch=='-')||(ch=='*')||(ch=='/'))
		return true;
	return false;

}
int Priority(const char& ch){
	switch (ch)
	{
	case '(':return 0;
	case ')':return 1;
	case '+':return 2;
	case '-':return 2;
	case '*':return 3;
	case '/':return 3;
	
	default: return -1;
	}


}
//((1+31)/2+6.5)*(4.8+5)
int ErrorCheck (const string &str, bool print){
	TStack BracStack(MAXSTRSIZE);
	int ArrBrac [MAXSTRSIZE][2]={0};
	int ArrCoun =0; //���� ����� �������, ������� �� ������ ���������
	int BracCoun=1;//������ ������ ������������� �����
	int i=0;
	int ErrCoun=0;//������� ������
	while (str[i]){

		
		if(str[i]=='('){
			BracStack.Put(BracCoun);
			BracCoun++;
			
			
		}
		else if(str[i]==')'){
			
			if (!BracStack.IsEmpty()){
				ArrBrac[ArrCoun][0]=BracStack.Get();
				ArrBrac[ArrCoun][1]=BracCoun;

				//cout<<ArrBrac[ArrCoun][0]<<" "<<ArrBrac[ArrCoun][0]<<endl;
				BracCoun++;
				ArrCoun++;
			}else{
			ArrBrac[ArrCoun][0]=0;
			ArrBrac[ArrCoun][1]=BracCoun;
			BracCoun++;
			ArrCoun++;
			ErrCoun++;
		}}
		
		
		

		
		
		i++;
	}
	
	while(!BracStack.IsEmpty()){
			ArrBrac[ArrCoun][0]=BracStack.Get();
			ArrBrac[ArrCoun][1]=0;
			ArrCoun++;
			ErrCoun++;
		}
		
		if(print){
			if(ErrCoun!=0)
				cout<<ErrCoun<<" Bracket's errors"<<endl;
			else 
				cout << "Bracket errors not found" << endl;


			i=0;
			cout <<"|Open Brackets|\t|Close Brackets|"<<endl;
			while (i<ArrCoun){
				cout <<"\t"<<ArrBrac[i][0]<<"\t\t"<<ArrBrac[i][1]<<endl;
				i++;
			}


		}
		
		return ErrCoun;
}


string Transform(const string &str){
	if(ErrorCheck(str,false)==0){
	
	int i=0;
	TStack OpStack(MAXSTRSIZE);
	string result;
	while (str[i]){
		//���� ��������
		if(IsOperation(str[i])){
			if (str[i]=='('){//���� ������������� ������, �� ������ � ����
				OpStack.Put(str[i]);
			}else if (str[i]==')'){//���� �������������, �������� ��� �� ���� ������
				while(true){
					if (OpStack.ShowLast()=='('){
						OpStack.Get();
						break;
					}
					result+=(char)OpStack.Get();
				}
			
			}else if(OpStack.IsEmpty()){//���� ���� ����
				OpStack.Put(str[i]);
				
			}else if(Priority(str[i])>Priority((char)OpStack.ShowLast())){//���� ��������� �������� � ������ ���� ��� ����� ���������� ��������� �������� � �����
				OpStack.Put(str[i]);
				
			}else{//���� ��������� �������� � ����� ������, ��� � ������
				while(true){
				
					result+=(char)OpStack.Get();
					if(Priority((char)OpStack.ShowLast())<Priority(str[i])||OpStack.IsEmpty())
						break;
					}
				OpStack.Put(str[i]);
				}
			
			}
		else if(isdigit(str[i])){//���� �����, �� ������ ������ � ������
		result+=str[i];
		}
			i++;
	}
			
	//���� � ����� �������� ��������, �� ���� ������
	while (!OpStack.IsEmpty())
		result+=(char)OpStack.Get();
			
			
	return result;
	}else{
	throw 1;
	return "Error";
	}
}


//5 + 4 * (7 - 2)
int Calculation(const string & str){
	string post= Transform(str);
	TStack CalcStack(MAXSTRSIZE);
	int i=0;
	int FirstNum, SecNum;

	if(post!="Error"){
	while (post[i]){
		if(isdigit(post[i])){
			CalcStack.Put(post[i]-'0');
		}
		if(IsOperation(post[i])){
			SecNum = CalcStack.Get();
			FirstNum =CalcStack.Get();
			switch(post[i]){
			case '+':CalcStack.Put(FirstNum+SecNum);break;
			case '-':CalcStack.Put(FirstNum-SecNum);break;
			case '*':CalcStack.Put(FirstNum*SecNum);break;
			case '/':CalcStack.Put(FirstNum/SecNum);break;
				}
			}
		i++;
		}
	
	return CalcStack.Get();
	
	
	
	}else{
		throw 1;
	}
}
```
__����� ��� �������� ������ ����������__

```c++
	#include <gtest/gtest.h>
#include "expressions.h"

TEST(BrControl, can_check_parentheses_placement)
{
    EXPECT_EQ(true, ErrorCheck("(1+2)/(3+4*6.7)-5.3*4.4", false));
}

TEST(BrControl, can_detect_incorrect_placement_of_parentheses)
{
    EXPECT_EQ(false, ErrorCheck("(a+b1)/2+6.5)*(4.8+(", false));
}

TEST(ConvertToPostfix, can_convert_an_expression_with_int_nums)
{
    EXPECT_EQ(Transform("3*(1-5)+7"), "315-*7+");
}

TEST(ConvertToPostfix, not_covnert_an_exp_with_incrrt_placement_of_pars)
{
    ASSERT_ANY_THROW(Transform("2*(8-5)+4)"));
}

TEST(Calculation, can_calculate_a_expression)
{
    int res = (1+2)/(3+4*6)-5*4;
    EXPECT_DOUBLE_EQ(Calculation("(1+2)/(3+4*6)-5*4"), res);
}

TEST(Calculation, not_calculate_an_exp_with_incorrect_placement_of_pars)
{
    ASSERT_ANY_THROW(Calculation("(1+2)(/())3+4*6.7)-5.3*4.4)("));
}
```


## ������������ �����������

  - ������� �������� ������ [Git][git].
  - ��������� ��� ��������� �������������� ������ [Google Test][gtest].
  - ����� ���������� Microsoft Visual Studio 2013.

##�����  

��� ���� ������ ���� ����������, ������ ����� ��������, ��� ��� �������� ����������� � ���������� ������������� ����� ����� ������� ����� TStack ���������������.
�� ����� ���������� ������ �������� �������� � ����������� gtest, ������� ���� ������ ������ ��������� �����.

