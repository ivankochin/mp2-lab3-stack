#ifndef __TSTACK_H
#define __TSTACK_H

#include "tdataroot.h"
#include <stdio.h>
#include <iostream>


using namespace std;

class TStack: public TDataRoot{
	protected:
		int Top;
	public:
		TStack(int Size = DefMemSize): TDataRoot(Size), Top(-1){}
		virtual void Put(const TData &Val);
		virtual TData Get(void);
		virtual void Print();
		virtual int IsValid();
		int GetDataCount(){return DataCount;};
		TData ShowLast(void);
		};

//�������� � ����
void TStack::Put(const TData &Val)
{
	if(pMem == NULL)
		SetRetCode(DataNoMem);
	else if (IsFull()){
		void* p = nullptr;
		SetMem(p, MemSize + DefMemSize);
		pMem[++Top] = Val;
		DataCount++;
	}
	else
	{
		pMem[++Top]=Val;
		DataCount++;
	}
}
//������� �� �����
TData TStack::Get(void)
{
	TData result = -1;
	if(pMem == NULL)
		SetRetCode(DataNoMem);
	else if (IsEmpty())
		SetRetCode(DataEmpty);
	else
	{
		result = pMem[Top--];
		DataCount--;
	}
	return result;
}
//���������� ����
void TStack:: Print ()
{
	if(IsEmpty())
	cout<<"Stack is Empty";
	for (int i=0;i<DataCount;i++)
		cout<<pMem[i]<<' ';

	cout<<endl;


}
int TStack:: IsValid()
{
	return 1;
}

TData TStack:: ShowLast(void){
	TData result = -1;
	if(pMem == NULL)
		SetRetCode(DataNoMem);
	else if (IsEmpty())
		SetRetCode(DataEmpty);
	else
	{
		result = pMem[Top];
	}
	return result;
}

#endif